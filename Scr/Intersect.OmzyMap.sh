#!/bin/bash

BaseDir=$1

Nthreads=22
CallsDir=${BaseDir}"/VariantCalls"
OutPath=$CallsDir"/Intersect"

if [[ ! -d $OutPath ]]; then
    mkdir -p $OutPath
fi

# retrieve sample name from FreeB folder
# Ind1 is FreeB vcf
# Ind2 is SAMt vcf
# Ind3 the path to Intersect output
for Ind1 in $(ls ${CallsDir}/FreeB/*vcf.gz) 
do
  if (( i % Nthreads == 0 )); then
    wait
  fi
  ((i++))
  (
  Ind2=$(echo $Ind1 | sed "s/FreeB/SAMt/")
  Ind3=$(echo $Ind1 | sed "s/FreeB/Intersect/")
  vcf-isec -n +2 $Ind2 $Ind1 | bgzip > $Ind3  
  ) &
done

wait



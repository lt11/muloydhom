#!/bin/bash

Ref1Label="DBVPG6765"
Ref1="WE"
HybridZero="A887R37"

############################################

BaseDir=$(dirname "$(pwd)")
cd $BaseDir/VariantCalls/Effect
OutDir="../Unique"
rm -rf $OutDir
mkdir $OutDir

for IndS in $(ls *vcf | grep -v ParHyb | grep -v $HybridZero | cut -d"." -f1 | sort | uniq)
do
  # per togliere il \t alla fine dell'header
  grep ^# $IndS.$Ref1.Ann.vcf | sed 's/\t$//' > $OutDir/$IndS.$Ref1Label.vcf
  grep -v ^# $IndS.$Ref1.Ann.vcf >> $OutDir/$IndS.$Ref1Label.vcf
  bgzip $OutDir/$IndS.$Ref1Label.vcf
  tabix -f $OutDir/$IndS.$Ref1Label.vcf.gz
done

cd $OutDir
AllFiles=$(ls *vcf.gz)

bcftools merge --output-type v $AllFiles | grep -v "^##" | bgzip > Merged.txt.gz

Rscript $BaseDir"/Scr/UniqueVar.Omzy.R" $BaseDir

rm -f *.vcf.gz*




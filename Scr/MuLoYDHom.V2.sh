#!/bin/bash

#####################
### user settings ###
#####################

Ref1Label="SGD"
Ref1="SGD"
ParentalZero="-"
HybridZero="skip"

#####################
#####################
#####################

### part 0

# base folder
BaseDir=$(dirname "$(pwd)")
# path to SnpEff.jar
SnpEff="/home/ltattini/Tools/snpEff/snpEff.jar"
# check Logs folder
if [[ ! -d Logs ]]; then mkdir Logs; fi

### part I: reference & annotation initialization

mkdir -p "${BaseDir}"/Ref/Ann
cp "${BaseDir}"/Rep/Asm/"${Ref1Label}".genome.fa "${BaseDir}"/Ref
cp "${BaseDir}"/Rep/Ann/"${Ref1Label}".* "${BaseDir}"/Ref/Ann

### part II: mappings

bash QualCheck1.sh "${BaseDir}" &> Logs/QualCheck1.log &

/usr/bin/time -v Rscript FormatRef.OmzyMap.R "${Ref1Label}" "${BaseDir}"/Ref > Logs/FormatRef.OmzyMap.out 2> Logs/Time.FormatRef.OmzyMap.err

(
/usr/bin/time -v bash Index.BWA.sh "${BaseDir}" > Logs/Index.BWA.out 2> Logs/Time.Index.BWA.err &

/usr/bin/time -v bash Index.SAMt.sh "${BaseDir}" &> Logs/Index.SAMt.log &

wait
)

/usr/bin/time -v bash BWA.Mapping.sh "${BaseDir}" > Logs/BWA.Mapping.out 2> Logs/Time.BWA.Mapping.err

/usr/bin/time -v bash SAMt.Dup.sh "${BaseDir}" > Logs/SAMt.Dup.out 2> Logs/Time.SAMt.Dup.err

wait

(
/usr/bin/time -v bash CalcDepth.Raw.sh "${BaseDir}" > Logs/CalcDepth.Raw.out 2> Logs/Time.CalcDepth.Raw.err &

/usr/bin/time -v bash CalcDepth.Ddp.sh "${BaseDir}" > Logs/CalcDepth.Ddp.out 2> Logs/Time.CalcDepth.Ddp.err &

wait

/usr/bin/time -v Rscript DepthParser.R "${BaseDir}" > Logs/DepthParser.out 2> Logs/Time.DepthParser.err &
) &

bash QualCheck2.sh "${BaseDir}" &> Logs/QualCheck2.log &

### part III: variant calling

(
## small variants
(
/usr/bin/time -v bash SAMt.Call.OmzyMap.sh "${BaseDir}" > Logs/SAMt.Call.OmzyMap.out 2> Logs/Time.SAMt.Call.OmzyMap.err &

/usr/bin/time -v bash FreeBayes.Call.OmzyMap.sh "${BaseDir}" > Logs/FreeBayes.Call.OmzyMap.out 2> Logs/Time.FreeBayes.Call.OmzyMap.err &

wait

/usr/bin/time -v bash Intersect.OmzyMap.sh "${BaseDir}" > Logs/Intersect.OmzyMap.out 2> Logs/Time.Intersect.OmzyMap.err

/usr/bin/time -v bash SubtractPar.OmzyMap.sh "${Ref1Label}" "${ParentalZero}" "${BaseDir}" > Logs/SubtractPar.OmzyMap.out 2> Logs/Time.SubtractPar.OmzyMap.err

/usr/bin/time -v bash SubtractHyb.OmzyMap.sh "${Ref1Label}" "${HybridZero}" "${BaseDir}" > Logs/SubtractHyb.OmzyMap.out 2> Logs/Time.SubtractHyb.OmzyMap.err
) &

wait

/usr/bin/time -v Rscript VariantParser.R "${BaseDir}" > Logs/VariantParser.out 2> Logs/Time.VariantParser.err
) &

## copy numbers

(
/usr/bin/time -v bash Mappability.GEM.sh "${Ref1Label}" "${BaseDir}" > Logs/Mappability.GEM.out 2> Logs/Time.Mappability.GEM.err

/usr/bin/time -v bash PreControl-FREEC.sh "${Ref1Label}" "${BaseDir}" > Logs/PreControl-FREEC.out 2> Logs/Time.PreControl-FREEC.err

/usr/bin/time -v bash Control-FREEC.sh "${BaseDir}" > /dev/null 2> Logs/Time.Control-FREEC.err
) &

wait

### part IV: variant annotations

/usr/bin/time -v Rscript Filter.Regions.SmallVar.R ${Ref1Label} ${Ref1} ${BaseDir} > Logs/Filter.Regions.SmallVar.out 2> Logs/Time.Filter.Regions.SmallVar.err

# /usr/bin/time -v bash SnpEff.OmzyMap.sh "${Ref1Label}" "${Ref1}" "${SnpEff}" "${BaseDir}" > Logs/SnpEff.OmzyMap.out 2> Logs/Time.SnpEff.OmzyMap.err &

wait

rm -rf "${BaseDir}"/MapRaw




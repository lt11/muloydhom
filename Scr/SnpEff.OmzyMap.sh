#!/bin/bash

################################################
# annota le varianti nella cartella 
# "Region" con SnpEff
# poi fa lo stesso con "Intersect" ma 
# l'output è una sottocartella
# (meno visibile)

# occhio: crossref!
################################################

Ref1Label=$1
Ref1=$2
SnpEff=$3
BaseDir=$4

Ref1Version="1"
SnpEffConfig=$(echo $SnpEff | sed 's|...$|config|')

WorkDir=$BaseDir"/VariantCalls/Regions"
OutDir=$BaseDir"/VariantCalls/Effect"

if [[ ! -d $OutDir ]]; then
    mkdir -p $OutDir
fi

cd $WorkDir
# occhio, questi vcf NON sono compressi
for IndSamp in $(ls *.$Ref1.vcf | grep -v "Ann")
do
  SampName=$(echo $IndSamp | cut -d"." -f1)
  # ricerco nel file config il nome del genoma e la versione da dare in pasto a SnpEff
  Ref1Name=$(grep $Ref1Label $SnpEffConfig | grep -v ^# | cut -d" " -f1 | grep "\.$Ref1Version\.genome$" | sed 's|\.genome||')
  java -Xmx4g -jar $SnpEff -v $Ref1Name -s $SampName.$Ref1.Summary.html $IndSamp > $SampName.$Ref1.Ann.vcf
  mv $SampName.$Ref1.Ann.vcf $SampName.$Ref1.Summary* $OutDir
done &

WorkDir=$BaseDir"/VariantCalls/Intersect"
OutDir=$BaseDir"/VariantCalls/Intersect/Effect"

if [[ ! -d $OutDir ]]; then
    mkdir -p $OutDir
fi

cd $WorkDir
# occhio, questi vcf sono compressi e contengono le varianti su entrambi i reference
for IndSamp in $(ls *.vcf.gz | grep -v "Ann")
do
  SampName=$(echo $IndSamp | cut -d"." -f1)
  # tolgo dal vcf le varianti e le stringhe contig nell'header dell'altro reference col grep -v
  # tolgo _RefNLabel dalle entry del campo CHROM (e dall'header) con sed (sennò il DB di SnpEff si incazza)
  zcat $IndSamp | sed "s|_${Ref1Label}||g" > $SampName.Ref1.vcf.tmp
  # ricerco nel file config il nome del genoma e la versione da dare in pasto a SnpEff
  Ref1Name=$(grep $Ref1Label $SnpEffConfig | grep -v ^# | cut -d" " -f1 | grep "\.$Ref1Version\.genome$" | sed 's|\.genome||')
  java -Xmx4g -jar $SnpEff -v $Ref1Name -s $SampName.$Ref1.Summary.html $SampName.Ref1.vcf.tmp > $SampName.$Ref1.Ann.vcf
  mv $SampName.$Ref1.Ann.vcf $SampName.$Ref1.Summary* $OutDir
  rm $SampName.Ref1.vcf.tmp
done &

wait




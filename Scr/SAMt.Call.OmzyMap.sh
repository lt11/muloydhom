#!/bin/bash

BaseDir=$1

Nthreads=22
BamDir=${BaseDir}"/MapDdp"
DataExt=".rmd.bam"
OutDir=${BaseDir}"/VariantCalls"

if [[ ! -d $OutDir ]]; then
    mkdir -p $OutDir
fi

OutSAMt=$OutDir"/SAMt"
if [[ ! -d $OutSAMt ]]; then
    mkdir -p $OutSAMt
fi

cd $BamDir

for Ind1 in $(ls *${DataExt}) 
do
  if (( i % Nthreads == 0 )); then
    wait
  fi
  ((i++))
  (
  RefPath=$(samtools view -H $Ind1 | grep "^@PG" | head -1 | cut -f 5 | cut -d " " -f 6)
  RefName=$(basename $RefPath | cut -d "." -f 1)
  SampleName=$(echo $Ind1 | cut -d "." -f 1)
  
  samtools mpileup -u --adjust-MQ 50 -min-MQ5 --output-tags AD,ADF,ADR,DP,SP --redo-BAQ -f $RefPath $Ind1 | bcftools call -mv -Oz > $OutSAMt"/"$SampleName"."$RefName".vcf.gz"
  tabix -p vcf $OutSAMt"/"$SampleName"."$RefName".vcf.gz"
  ) &
done

wait




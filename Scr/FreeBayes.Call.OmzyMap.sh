#!/bin/bash

BaseDir=$1

Nthreads=22
BamDir=${BaseDir}"/MapDdp"
DataExt=".rmd.bam"
OutDir=${BaseDir}"/VariantCalls"

if [[ ! -d $OutDir ]]; then
    mkdir -p $OutDir
fi

OutFreeB=$OutDir"/FreeB"
if [[ ! -d $OutFreeB ]]; then
    mkdir -p $OutFreeB
fi

cd $BamDir

for Ind1 in $(ls *${DataExt})
do
  if (( i % Nthreads == 0 )); then
    wait
  fi
  ((i++))
  (
  SampleName=$(echo $Ind1 | cut -d "." -f 1)
  RefPath=$(samtools view -H $Ind1 | grep "^@PG" | head -1 | cut -f 5 | cut -d " " -f 6)
  RefName=$(basename $RefPath | cut -d"." -f 1)
  
  freebayes -p 2 --fasta-reference $RefPath $Ind1 > $OutFreeB"/"$SampleName"."$RefName".vcf"
  # fix sample name
  sed "s|unknown|$Ind1|" $OutFreeB"/"$SampleName"."$RefName".vcf" > $OutFreeB"/"$SampleName"."$RefName".temp.vcf"
  mv -f $OutFreeB"/"$SampleName"."$RefName".temp.vcf" $OutFreeB"/"$SampleName"."$RefName".vcf"
  bgzip $OutFreeB"/"$SampleName"."$RefName".vcf"
  tabix -p vcf $OutFreeB"/"$SampleName"."$RefName".vcf.gz"
  ) &
done

wait




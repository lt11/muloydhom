#!/bin/bash

################################################
# non contempla l'opzione $H0Label == "-"
# per introdurla c'è da cambiare lo script
# FiltSharedVar.R
################################################

Ref1Label=$1
H0Label=$2
BaseDir=$3

Nthreads=22

OutDir=$BaseDir"/VariantCalls/HybSubtracted"
InputDir=$BaseDir"/VariantCalls/ParSubtracted"

if [[ ! -d $OutDir ]]; then
  mkdir -p $OutDir
else
  rm -rf $OutDir
  mkdir -p $OutDir
fi
cd $InputDir
# per evitare che si pianti nei re-run
# Merged.txt.gz causa un segmentation fault
rm -f $InputDir/*tbi $InputDir/Merged.txt.gz
if [[ $H0Label == "skip" ]]; then
  cp $InputDir/*vcf.gz $OutDir
else
  # apro tutti i gz dei controlli per correggere il nome del campione
  for IndH0 in $H0Label
  do
    gunzip $IndH0.$Ref1Label.vcf.gz
  done

  for File1 in $(ls *vcf.gz)
  do
    if (( i % Nthreads == 0 )); then
      wait
    fi
    ((i++))
    (
    File1ID=$(echo $File1 | cut -d "." -f 1)
    # correzione nome campione
    for IndH0 in $H0Label
    do
      sed "s|$IndH0|$File1ID|g" < $IndH0.$Ref1Label.vcf > $IndH0.temp.4.$File1ID.vcf
    done
    bgzip *temp.4.$File1ID.vcf
    tabix *temp.4.$File1ID.vcf.gz
    tabix -f $File1
    # sottrazione varianti
    vcf-isec -c $File1 *temp.4.$File1ID.vcf.gz | bgzip > $OutDir"/"$File1
    rm -f *temp.4.$File1ID*
    ) &
  done
  
  wait
  
  for IndH0 in $H0Label
  do
    bgzip $IndH0.$Ref1Label.vcf
    cp $IndH0.$Ref1Label.vcf.gz $OutDir
  done
fi




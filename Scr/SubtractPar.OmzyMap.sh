#!/bin/bash

Ref1Label=$1
ParLabel=$2
BaseDir=$3

Nthreads=22

OutDir=$BaseDir"/VariantCalls/ParSubtracted"
InputDir=$BaseDir"/VariantCalls/Intersect"

IndR=$Ref1Label

# per evitare che si pianti nei re-run
rm -f $InputDir/*tbi

if [[ ! -d $OutDir ]]; then
  mkdir -p $OutDir
fi

if [[ $ParLabel == "-" ]]; then
  cp $InputDir/*.vcf.gz $OutDir
else
  cd $InputDir
  # espandi il file Par, poi leggi i vcf non espansi
  gunzip $ParLabel.$IndR.vcf.gz
  # leggi i vcf non espansi
  for File1 in $(ls *vcf.gz | grep $IndR)
  do
    if (( i % Nthreads == 0 )); then
      wait
    fi
    ((i++))
    (
    File1ID=$(echo $File1 | cut -d "." -f 1)
    sed "s|$ParLabel|$File1ID|g" < $ParLabel.$IndR.vcf > temp.4.$File1ID.vcf
    bgzip temp.4.$File1ID.vcf
    tabix -f temp.4.$File1ID.vcf.gz
    tabix -f $File1
    vcf-isec -c $File1 temp.4.$File1ID.vcf.gz | bgzip > $OutDir"/"$File1
    rm -f temp.4.$File1ID*
    ) &
  done
  
  wait
  bgzip $ParLabel.$IndR.vcf
  cp $ParLabel.$IndR.vcf.gz $OutDir
fi




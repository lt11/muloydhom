#!/bin/bash

Ref1=$1
BaseDir=$2

Nthreads=22
CopyNumDir=${BaseDir}"/CNV"
MapDir=${BaseDir}"/CNV/Mappability"
ModDir=${BaseDir}"/Ref/Mod"

AllRef="${Ref1}"

if [[ ! -d ${MapDir} ]]; then
    mkdir -p ${MapDir}
fi

for IndRef in ${AllRef}
do
  WorkDir=$MapDir"/"$IndRef
  if [[ ! -d ${WorkDir} ]]; then
    mkdir -p ${WorkDir}
  fi
  
  cp $ModDir"/"$IndRef".genome.chrref.fa" $WorkDir
  cd $WorkDir
  
  gem-indexer -T $Nthreads -c dna -i $IndRef".genome.chrref.fa" -o $IndRef
  gem-mappability -T $Nthreads -I  $IndRef".gem" -l 150 -o  $IndRef
  # output $IndRef.mappability
  rm -f $IndRef".genome.chrref.fa"
done



